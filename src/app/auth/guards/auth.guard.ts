import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
/**Responsible for intercepting Http requests, from the application,
 * to the backend, inserting the Authorization property with the
 * Token in the header
 */
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /**If an Http request to that URL is not being made,
     * continue making the request, but without injecting
     * the Token
     */
    if (
      !request
      || !request.url
      || (request.url.startsWith('http'))
      && !(environment.END_POINT && request.url.startsWith(environment.END_POINT))
      ) {
        return next.handle(request);
    }
    /**Retrieves the Token that is stored
     * in the browser microservices
     */
    const token = localStorage.getItem('token')
    || sessionStorage.getItem('token');
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });
    }
    return next.handle(request);
  }
}
