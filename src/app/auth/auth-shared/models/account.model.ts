export interface IAccount {
  id?: number;
  activated?: boolean;
  authorities?: string[];
  email?: string;
  firstName?: string;
  lastName?: string;
  login?: string;
}

export class Account implements IAccount {
    constructor(
      public id?: number,
      public activated?: boolean,
      public authorities?: string[],
      public email?: string,
      public firstName?: string,
      public lastName?: string,
      public login?: string
    ) {}
  
}
