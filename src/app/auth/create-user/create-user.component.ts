import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IAccount } from '../auth-shared/models/account.model';
import { AccountService } from '../account.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.sass']
})
export class CreateUserComponent implements OnInit {
  user: IAccount;
  saved = false;

  registerForm = this.fb.group({
    login: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(12)
    ]],
    firstName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    lastName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    email: ['', [
      Validators.required,
      Validators.email,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),
    ]],
    activated: [false],
    authorities: ['']
  });
  
  constructor(
    private fb: FormBuilder,
    private accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  registerUser():void {
    this.user = this.registerForm.value;
    this.saved = true;
    this.accountService.createUser(this.user)
      .subscribe((response) => {
        this.saved = true;
        Swal.fire('User created successfully', 'Thanks for the registration', 'success');
        this.router.navigate(['/dashboard']);
        return response;
      } ,(error) => {
        Swal.fire('The user was not created', 'Please verify the data', 'error');
    });
  }
  

}
