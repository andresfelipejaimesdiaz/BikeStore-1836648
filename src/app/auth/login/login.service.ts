import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { Account } from '../auth-shared/models/account.model';
import { ICredentials } from '../auth-shared/models/credentials';
import { flatMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AccountService } from '../account.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private authService: AuthService,
    private accountService: AccountService
  ) { }

  /**Solicita los datos del usuario para saber
   * cuando está autenticado (Account debe ser
   * importada desde la interfaz que creamos,
   * no la que viene por defecto)
   */
  public login(credentials: ICredentials): Observable<Account | null> {
    return this.authService.login(credentials)
    .pipe(flatMap(() => this.accountService.identity(true)));
  }

  public logout(): void {
    this.authService.logout()
    .subscribe(null, null, () => this.accountService.authenticate(null));
  }

}
