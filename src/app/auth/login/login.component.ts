import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { LoginService } from './login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    username: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(8)
    ]],
    password: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    rememberMe: [false]
  });

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    // private router: Router
  ) { }

  ngOnInit(): void {

  }

  login(): void {
    /*Variable "credentials" of type interface ICredentials
    that will be an instance of the class Credentials*/
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.loginService.login(credentials)
      .subscribe((res: any) => {
        Swal.fire('Successful login', 'Welcome to BikeStore', 'success');
        // this.accessTrue();
      }, (error: any) => {
        if (error.status === 401) {
          Swal.fire('User or Password Invalid', ' ', 'error');
          // this.accessDenied();
        }
      });
  }

  /*accessTrue(): void {
    setTimeout(() => {
      this.router.navigate(['/dashboard']);
    }, 3000);
  }*/
  /*accessDenied(): void {
    setTimeout(() => {
      this.router.navigate(['../../app-routing.module.ts/access-denied']);
    }, 3000);
  }*/
}
