import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import Swal from 'sweetalert2';
import { IAccount, Account } from '../auth-shared/models/account.model';
import { Authority } from '../auth-shared/constants/authority.constants';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.sass']
})
export class UpdateUserComponent implements OnInit {
  userData: Account | null;
  authority: Authority
  editForm = this.fb.group({
    id: [''],
    login: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(12)
    ]],
    firstName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    lastName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    email: ['', [
      Validators.required,
      Validators.email,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"),
    ]],
    activated: [''],
    authorities: ['']
  });

  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.accountService.identity()
    .subscribe((res: any) => {
      this.userData = res;
    });
    this.updateForm(this.userData);
  }

  updateForm(account: Account | null): void {
    this.editForm.patchValue({
      id: account.id,
      login: account.login,
      firstName: account.firstName,
      lastName: account.lastName,
      email: account.email,
      authorities: account.authorities,
      activated: account.activated
    })
  }

  updateAccount(): IAccount {
    return {
      ...new Account(),
      id: this.editForm.get('id').value,
      login: this.editForm.get('login').value,
      firstName: this.editForm.get('firstName').value,
      lastName: this.editForm.get('lastName').value,
      email: this.editForm.get('email').value,
      authorities: this.editForm.get('authorities').value,
      activated: this.editForm.get('activated').value
    };
  }

  updateData(): void {
    this.accountService.updateUser(this.updateAccount())
    .subscribe(res => {
      Swal.fire('User updated successfully', '', 'success');
      this.router.navigate(['/dashboard']);
      if (this.updateData) {
        this.ngOnInit();
        console.log('User update');
      } else {
        console.log('User Not Update');
      }
    }, error => error);
  }

}
