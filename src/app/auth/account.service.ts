import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account, IAccount } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { tap, shareReplay, catchError, map } from 'rxjs/operators';
import { ICredentials } from './auth-shared/models/credentials';
import { Authority } from './auth-shared/constants/authority.constants';

@Injectable({
  providedIn: 'root'
})
/** Provee la información a la aplicación para saber si el usuario
 * está autenticado, saber su rol y se encarga de registrar nuevos usuarios
 */
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router,
    
  ) { }

  create(credentials: ICredentials): Observable<ICredentials> {
    return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, credentials);
  }
   
  createUser(user: IAccount): Observable<IAccount> {
    return  this.http.post<IAccount>(`${environment.END_POINT}/api/users`, user )
  }
  /*registerUser(user: IAccount): Observable<IAccount> {
    return  this.http.post<IAccount>(`${environment.END_POINT}/api/register`, user )
  }*/
  updateUser(user: IAccount): Observable<IAccount> {
    return  this.http.put<IAccount>(`${environment.END_POINT}/api/users`, user )
  }

  identity(force?: boolean): Observable<Account | null> {
    if (
      !this.accountCache
      || force
      || !this.isAuthenticated()
      ) {
        this.accountCache = this.fetch()
        .pipe(catchError(() => {
          return of(null);
        }), tap((account: Account | null) => {
            this.authenticate(account);
            if (account) {
              this.router.navigate(['/dashboard']);
            }
        }), shareReplay());
    }

    return this.accountCache;
  }

  authenticate(account: Account | null): void {
    this.userIdentity = account;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getUserName(): string {
    return this.userIdentity.firstName + ' ' + this.userIdentity.lastName;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  // Verify Rols

  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }

    if (!Array.isArray(authorities)) {
      authorities = [authorities];
    }

    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  private fetch(): Observable<Account> {
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }

}
