import { Component, OnInit } from '@angular/core';
import { Account, IAccount } from '../auth-shared/models/account.model';
import { AccountService } from '../account.service';
import { FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.sass']
})
export class ProfileUserComponent implements OnInit {
  userData: Account | null;
  editForm = this.fb.group({
    id: [''],
    login: [''],
    firstName: [''],
    lastName: [''],
    email: [''],
    activated: [''],
    authorities: ['']
  });
  
  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.accountService.identity()
    .subscribe((res: any) => {
      this.userData = res;
    });
    this.updateForm(this.userData);
    
  }

  updateForm(account: Account | null): void {
    this.editForm.patchValue({
      id: account.id,
      login: account.login,
      firstName: account.firstName,
      lastName: account.lastName,
      email: account.email,
      authorities: account.authorities,
      activated: account.activated
    })
  }

  updateAccount(): IAccount {
    return {
      ...new Account(),
      id: this.editForm.get('id').value,
      login: this.editForm.get('login').value,
      firstName: this.editForm.get('firstName').value,
      lastName: this.editForm.get('lastName').value,
      email: this.editForm.get('email').value,
      authorities: this.editForm.get('authorities').value,
      activated: this.editForm.get('activated').value
    };
  }

  redirectUpdate(): void {
    this.router.navigate(['dashboard/update']);
  }

}
