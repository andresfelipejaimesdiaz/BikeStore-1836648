import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { UserRouteAccessService } from '../auth/guards/user-route-access.service';
import { CreateUserComponent } from '../auth/create-user/create-user.component';
import { ProfileUserComponent } from '../auth/profile-user/profile-user.component';
import { UpdateUserComponent } from '../auth/update-user/update-user.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'bikes',
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/bikes/bikes.module')
        .then(m => m.BikesModule)
      },
      {
        path: 'sales',
        data: {
          authorities: [Authority.USER]
        },
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/sales/sales.module')
        .then(m => m.SalesModule)
      },
      {
        path: 'register',
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccessService],
        component: CreateUserComponent
      },
      {
        path: 'profile',
        data: {
          authorities: [Authority.ADMIN, Authority.USER]
        },
        canActivate: [UserRouteAccessService],
        component: ProfileUserComponent
      },
      {
        path: 'update',
        data: {
          authorities: [Authority.USER, Authority.ADMIN]
        },
        canActivate: [UserRouteAccessService],
        component: UpdateUserComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
