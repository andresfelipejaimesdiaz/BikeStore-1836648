import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/auth/account.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/auth/login/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  userData: string;

  constructor(
    private accountService: AccountService,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getNameUser();
  }

  getNameUser(): void {
    this.userData = this.accountService.getUserName();
  }

  logout(): void {
    this.loginService.logout();
    Swal.fire('Session closed successfully', 'Come back soon', 'success');
    this.router.navigate(['/login']);
  }

}
